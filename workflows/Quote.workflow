<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <fieldUpdates>
        <fullName>AvaTax_Update_Tax_Date</fullName>
        <field>Tax_Date__c</field>
        <formula>CreatedDate</formula>
        <name>AvaTax - Update Tax Date</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <rules>
        <fullName>AvaTax - Update Quote Tax Date</fullName>
        <actions>
            <name>AvaTax_Update_Tax_Date</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <formula>ISBLANK (CreatedDate) = False</formula>
        <triggerType>onCreateOnly</triggerType>
    </rules>
</Workflow>
