public with sharing class MapperUIController {    
    public static String jsonData{get;set;}
    
    public static String classContent{get;set;}
    
    public static String className {get;set;}
    
    public static String classResponse{get;set;}
    
    public static String xmlString{get;set;}
    
    public list<ObjectClass> allObjList{get;set;}
    
    public list<String> allobjectList{get;set;}
    
    public set<string> objectSet{get;set;}
    
    final static String ENDPOINT = URL.getSalesforceBaseUrl().toExternalForm()+'/services/data/v28.0/tooling/sobjects';
    
    @remoteAction
    public static list<String> displayObjectDetail(){
        list<String> allobjectList = new list<String> (); 
        list<schema.sObjectType> allObjects = Schema.getGlobalDescribe().Values(); 
        
        for(Schema.SObjectType objTyp : Schema.getGlobalDescribe().Values()){
            Schema.DescribeSObjectResult objResult = objTyp.getDescribe();
            if(objResult.isAccessible() == True && objResult.isUpdateable() == True && !objResult.isCustomSetting() &&
               objResult.getRecordTypeInfos().size() > 0 && objResult.isCreateable() == True && objResult.isDeletable() == True){
                   allobjectList.add(objResult.getName());
               } 
        }
        allObjectList.sort();
        return allObjectList;
    }
    
    
    @remoteAction
    public static String loadXml()
    {  
        try{
            Type customType  = Type.forName('SalesCloudUIMapperConfiguration');  
            if(null != customType){
                AVA_MAPPER.IMapperConfiguration imc = (AVA_MAPPER.IMapperConfiguration)customType.newInstance();
                AVA_MAPPER.MapperConfigurationResult mcr = imc.readData();
                if(mcr.statusCode == 200){
                    return mcr.xmlData;
                } 
            }
        }
        catch(exception e) {
            system.debug('exception'+e.getStackTraceString());
        }
        return null;
    }
    
    @remoteAction
    public static String generateNewHookClass(String className,String classContent)
    { 
        HttpRequest req = createHttpRequest(ENDPOINT+'/ApexClass','POST');
        req.setBody( '{"Name":"'+ className +'","Body":"'+ classContent.escapeUnicode() +'"}');
        classResponse = getResponse(req);
        return classResponse;  
    }
    
    // used receive response by sending a http request
    private static String getResponse(HttpRequest req)
    {
        try
        {
            Http httpreq = new Http();
            HttpResponse res = httpreq.send(req);
            String reqresponse = res.getBody();
            return reqresponse;
        }
        catch (Exception e)
        {
            return 'Error:' +e.getMessage();
        }
    }
    
    // create a http request with required endpoint and request method
    private static HttpRequest createHttpRequest(String endpoint, String method)
    {
        HttpRequest req = new HttpRequest();
        endpoint += '';
        req.setHeader('Authorization', 'Bearer ' + UserInfo.getSessionID());
        req.setHeader('Content-Type', 'application/json');
        req.setEndpoint(endpoint);
        req.setMethod(method);
        return req;
    }
    
    @RemoteAction
    public static String getXmlString(String xmlString) {
        String result;
        if(xmlString != null){
            result = 'Received';
        }
        Type t = Type.forName('SalesCloudUIMapperConfiguration'); 
        if(null != t){
            AVA_MAPPER.IMapperConfiguration imc = (AVA_MAPPER.IMapperConfiguration)t.newInstance();
            AVA_MAPPER.MapperConfigurationResult mcr = imc.writeData(xmlString);
        }
        return result;
    }
    
    @RemoteAction
    public static String verifySql(String queryString) {
        String result;
        try{
            List<SObject> queryResults = Database.query(queryString);
            if(queryResults != null) {
                result = 'Pass';
            }
        } catch(exception e){
            result = e.getMessage();
        }
        return result;
    }
    
    @RemoteAction
    public static string fetchOrgNamespace(){
        string np = [SELECT NamespacePrefix FROM Organization LIMIT 1].NamespacePrefix;
        return np;
    }
    
    @RemoteAction
    public static List<String> getListofUpdateTaxHelperNames(){
        String nameSpace = [Select namespaceprefix from organization LIMIT 1].namespaceprefix;
        List<ApexClass> listApexClasses = [Select Name,NamespacePrefix From ApexClass Where namespaceprefix = 'AVA_SFCLOUD' Or namespaceprefix = : nameSpace];
        List<String> classNames = New List<String>();
        for(ApexClass aClass : listApexClasses){
            Type t = Type.forName(aClass.Name);
            if(t != null){
                try{
                    Object obj = t.newInstance();
                    if (obj instanceof Ava_Mapper.IUpdateTaxHelper){
                        if(aClass.NamespacePrefix == 'AVA_SFCLOUD' || String.isNotBlank(nameSpace))
                        {
                            classNames.add(aClass.NamespacePrefix+'.'+aClass.Name);
                        }
                        else
                        {
                            classNames.add(aClass.Name);
                        }
                    }
                }
                catch(exception e){
                }
            }
        }
        return classNames;
    }
    
}