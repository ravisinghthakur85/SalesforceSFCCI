/** Helper class for Subsidiary and AvaTax Company 
**/
public class MultiCompanyModel implements Comparable
{
    
  @AuraEnabled  public String sfCompany {get; set;}
  @AuraEnabled  public List<AvaTaxCompany__c> companyListing {get; set;}
  @AuraEnabled  public AvaTax__c avaTaxConfiguration {get; set;}
  @AuraEnabled  public String toolTipWarning {get; set;}
  @AuraEnabled  public String sfCompanyId {get; set;}
  @AuraEnabled  public String avaCompanyCode {get; set;}
  @AuraEnabled  public String customObjName {get; set;}
  @AuraEnabled  public String customFieldName {get; set;}
  @AuraEnabled  public String companyAddress{get;set;}  
  @AuraEnabled  public List<String> companyMapping{get;set;}  
  @AuraEnabled  public Boolean warningSignRender{
        get;
        set;
    }
    
    //Helper Method implementing IComparable
    public Integer compareTo(Object compareTo) {
        MultiCompanyModel compareToEmp = (MultiCompanyModel )compareTo;
        if (sfCompany == compareToEmp.sfCompany ) 
        {
            return 0;
        }
        if (sfCompany > compareToEmp.sfCompany ) 
        {
            return 1;
        }
        return -1;        
    }
}