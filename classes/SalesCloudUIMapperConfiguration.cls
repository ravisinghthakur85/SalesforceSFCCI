global with sharing class SalesCloudUIMapperConfiguration implements AVA_MAPPER.IMapperConfiguration {
    
    global AVA_MAPPER.MapperConfigurationResult writeData(String xmlData){
        
        AVA_MAPPER.MapperConfigurationResult mcr = new AVA_MAPPER.MapperConfigurationResult();
        try{
            String nameSpace = [Select namespaceprefix from organization].namespaceprefix;
            StaticResource sr = [SELECT id, body from StaticResource where name = 'SFConfig' and namespaceprefix =: nameSpace  LIMIT 1];
            string xmlFileName = 'SFConfig';
            string contentType = 'text/xml';
            string base64EncodedBody = EncodingUtil.base64Encode(Blob.valueof(xmlData));
            
            HttpRequest req = new HttpRequest();
            req.setEndpoint(System.Url.getSalesforceBaseURL().toExternalForm()+'/services/data/v29.0/tooling/sobjects/StaticResource/'+sr.Id+'?_HttpMethod=PATCH');
            req.setMethod('POST');
            req.setHeader('Content-Type', 'application/json');
            req.setHeader('Authorization', 'Bearer ' + UserInfo.getSessionId());
            // JSON formatted body
            req.setBody(
                '{"Name":"'+xmlFileName+'"'+
                ',"ContentType":"'+contentType+'"'+
                ',"Body":"'+base64EncodedBody+'"'+
                ',"CacheControl":"Public"}'
            );
            
            Http http = new Http();
            HttpResponse res = http.send(req);
            mcr.statusCode = res.getStatusCode();
        } catch(Exception ex){
            mcr.statusCode = 500;
            mcr.ErrorMesage = ex.getMessage();
        }
        return mcr;
    }
    
    global AVA_MAPPER.MapperConfigurationResult readData(){
        AVA_MAPPER.MapperConfigurationResult mcr = new AVA_MAPPER.MapperConfigurationResult();
        StaticResource resource = null;
        String nameSpace = [Select namespaceprefix from organization].namespaceprefix;
        try{
            if(Test.isRunningTest())
            {
                resource = [SELECT name, body from StaticResource where name = 'SFConfig' and namespaceprefix = : nameSpace LIMIT 0];
            }
            resource = [SELECT name, body from StaticResource where name = 'SFConfig' and namespaceprefix = : nameSpace LIMIT 1];
            mcr.statusCode = 200;
            mcr.xmlData = resource.body.toString();
        }
        catch(System.QueryException ex){
            //change the namespace prefix as per manage package
            resource = [select id, body from StaticResource where name = 'SFConfig' and namespaceprefix = 'AVA_SFCLOUD' Limit 1]; 
            string xmlFileName = 'SFConfig';
            string contentType = 'text/xml';
            string base64EncodedBody = EncodingUtil.base64Encode(Blob.valueof(resource.Body.toString()));
            
            HttpRequest req = new HttpRequest();
            req.setEndpoint(System.Url.getSalesforceBaseURL().toExternalForm()+'/services/data/v29.0/tooling/sobjects/StaticResource');
            req.setMethod('POST');
            req.setHeader('Content-Type', 'application/json');
            req.setHeader('Authorization', 'Bearer ' + UserInfo.getSessionId());
            // JSON formatted body
            req.setBody(
                '{"Name":"'+xmlFileName+'"'+
                ',"ContentType":"'+contentType+'"'+
                ',"Body":"'+base64EncodedBody+'"'+
                ',"CacheControl":"Public"}'
            );
            
            Http http = new Http();
            
            if(!Test.isRunningTest())
            {
                HttpResponse res = http.send(req);
                if(res.getStatusCode() == 201){
                    mcr.statusCode = 200;
                    mcr.xmlData = resource.body.toString();
                }else{
                    mcr.statusCode = 500;
                    mcr.ErrorMesage = ex.getMessage();
                }
            }
        }
        catch(Exception ex){
            mcr.statusCode = 500;
            mcr.ErrorMesage = ex.getMessage();
        }
        return mcr;
    }
}