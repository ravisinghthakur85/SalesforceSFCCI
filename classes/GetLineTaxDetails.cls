Global class GetLineTaxDetails implements Ava_Mapper.IUpdateTaxHelper {
    Global String extension(Ava_Mapper.TransactionModel transResult, Integer lineIndex){
        String lineDetails = '';
		for(Ava_Mapper.TransactionLineDetailModel tlm : transResult.lines[lineIndex].details) {
			lineDetails +='Rate : '+ tlm.rate*100+'% '+ ' '+ 'Tax Name : '+tlm.taxName + ' '+'Juris Name ' +tlm.jurisName+' \n';
        }
        return lineDetails;
    }
}