Global class GetShippingTax implements Ava_Mapper.IUpdateTaxHelper {
    Global String extension(Ava_Mapper.TransactionModel transResult, Integer lineIndex)
    {
        //Added below code for Shipping & Handling tax charges
        String shippingTax='';
        if(transResult.lines != null)
        {
            for(AVA_MAPPER.TransactionLineModel lineDetails : transResult.lines)
            {
                if(lineDetails.itemCode == AvaConstants.FREIGHT_ITEM_CODE)
                {
                    shippingTax = String.valueOf(lineDetails.tax);
                }
            }
        }
        return shippingTax;
    }
}