public class ObjectClass{
    public string label{get;set;}
    public string apiName{get;set;}
    public string keyPrefix{get;set;}
    public boolean isCustomSet{get;set;}
    public list<Schema.ChildRelationship> childObjects;
    public list<string> childObjectArray{get;set;}
    public list<schema.RecordTypeInfo> recordTypes;
    public list<string> recordTypeArray{get;set;}
    public ObjectClass(string lab, string api, string prefix, list<Schema.ChildRelationship> childObj, boolean isCustomSetting, list<schema.RecordTypeInfo> recordTypeList){
        this.label = lab;
        this.apiName = api;
        this.KeyPrefix = prefix;
        this.isCustomSet = isCustomSetting;
        this.childObjectArray = new list<string>();
        this.recordTypeArray = new list<string>();
        if(childObj!=null){
            for(schema.childRelationShip ch : childObj){
                this.childObjectArray.add(ch.getChildSObject().getDescribe().getLabel()+' ('+ch.getChildSObject().getDescribe().getName()+')');
            }
        }
        if(recordTypeList!= null){
            for(schema.RecordTypeInfo rt: recordTypeList){
                this.recordTypeArray.add(rt.getName());
            }
        }
    }
}