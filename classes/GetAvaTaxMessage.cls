Global class GetAvaTaxMessage implements Ava_Mapper.IUpdateTaxHelper {
    Global String extension(Ava_Mapper.TransactionModel transResult, Integer lineIndex)
    {
        String AvaTaxMsg = '';
        if(transResult.statusCode == 200 || transResult.statusCode == 201)
        {
            AvaTaxMsg = 'Sales Tax Current';
            if(AvaTax__c.getInstance(AvaConstants.AVATAX_CUSTOM_SETTING_NAME).Save_transactions_to_AvaTax__c == false && OrderTaxCalculator.orderId != null)
            {
            	AvaTaxMsg+='. Save Transactions To AvaTax option is disabled on the AvaTax Configuration. Changes are not reflected on the AvaTax Update';    
            }
        }
        else
        {
            //AvaTaxMsg = transResult.error.details[0].description;
            AvaTaxMsg = transResult.error.message;
        }
        return AvaTaxMsg;
    }
}