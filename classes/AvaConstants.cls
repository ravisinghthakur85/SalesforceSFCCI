public class AvaConstants {
    public static final String APP_NAME = 'SF Sales Cloud || 2.0';
    public static final String APP_VERSION = 'a0o0b000005ANemAAG';
    
    public static final Integer PRODUCT_DESCRPTION_LEN = 2048;
    public static final Integer ACCOUNT_NAME = 50;
    public static final Integer VAT_NUMBER_LEN = 25;    
    public static final Integer TAX_CODE_LEN = 25;
    public static final Integer ENTITY_USE_CODE_LEN = 25;
    public static final Integer ITEM_CODE = 50;
    public static final String AVATAX_CUSTOM_SETTING_NAME = 'AvaTax Config';
    public static final String AVATAX_ADDITIONAL_CUSTOM_SETTING_NAME = 'AvaTax';
    public static final String STATIC_RESOURCE_FILENAME = 'SFConfig';
    public static final String AVATAX_PACKAGE_NAMESPACE = 'AVA_SFCLOUD';
    public static final string AVATAX_CONNECTOR_NAME = 'AvaTax For SF Sales Cloud';
    public static final string AVATAX_CONNECTOR_VERSION = '2.0';
    public static final string AVATAX_LOGENTRIESTOKEN_PRODUCTION = '9218498b-561c-4a28-9b83-26f73b744673';
    public static final string AVATAX_LOGENTRIESTOKEN_SANDBOX = 'e970e777-3529-457d-8dac-f76193ea4302';
    public static final string TAX_CALCULATION_DISABLED = 'Tax calculation is disabled in AvaTax configuration';

    public static final string CONFIGURATION_CREDENTIAL_SAVE ='Configuration has been Saved!';
    public static final string INVALID_CREDENTIAL_AVATAX ='Please enter valid credentials!';
    public static final string CONNECTED_CREDENTIAL_AVATAX ='You are connected to AvaTax';
    public static final string ADDRESS_VALIDATION_DISABLED ='Error: Address validation is not enabled in configuration';

    public static final string TAX_OVERRIDE_REASON_AMOUNT = 'Tax Amount Override';
    public static final string TAX_OVERRIDE_REASON_DATE = 'Tax Date Override';

    public static final string AVATAX_CREDENTIALS_ERROR = 'AvaTax Account Id or License Key is missing';

    public static final string FREIGHT_ITEM_CODE = 'FREIGHT';
    public static final string FREIGHT_DESCRIPTION = 'S&H';
    public static final string FREIGHT_TAXCODE = 'FR';
    
    public static final string REQUIRED_CREDENTIAL_AVATAX ='Required Fields are missing';
    //public static final List<String> shippingCodeDescription = new List<String> {'Delivery by company vehicle before passage of title','After passage of title','Shipping Only (Not paid directly to common carrier)','Common carrier - FOB destination','Common carrier - FOB origin','Common carrier - FOB unknown','Non-common carrier - FOB destination','Non-common carrier - FOB origin','Non-common carrier - FOB unknown','Direct Mail-printed material for mass audience delivery','Charges that exceed the actual cost of delivery','Charges that exceed reasonable and prevailing rates','Shipping And Handling Combined','Common carrier - FOB destination','Common carrier - FOB origin','Common carrier - FOB unknown','Non-common carrier - FOB destination','Non-common carrier - FOB origin','Non-common carrier - FOB unknown', 'Direct Mail-printed material for mass audience delivery','Charges that exceed the actual cost of delivery','Charges that exceed reasonable and prevailing rates','Electronically Delivered','Temporary Unmapped Freight SKU - taxable default','Miscellaneous'};    
    //public static final List<String> shippingCodeName = new List<String> {'FR010000','FR010100','FR010200','FR020000','FR020100','FR020200','FR020300','FR020400','FR020500','FR020600','FR020700','FR020800','FR020900','FR030000','FR030100','FR030200','FR030300','FR030400','FR030500','FR030600','FR030700','FR030800','FR030900','FR040000','FR999999'};
}