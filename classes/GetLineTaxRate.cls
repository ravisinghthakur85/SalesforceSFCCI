Global class GetLineTaxRate implements Ava_Mapper.IUpdateTaxHelper {
    Global String extension(Ava_Mapper.TransactionModel transResult, Integer lineIndex){
        Decimal lineRate = 0.0;
        for(Ava_Mapper.TransactionLineDetailModel tlm : transResult.lines[lineIndex].details) {
            lineRate = lineRate+tlm.rate;
        }
        return String.valueOf(lineRate*100)+'%';
    }
}