global class ConfigurationProvider implements AVA_MAPPER.IConfigManager
{
    public AvaTaxConfigManager config = null;
    
    global AVA_MAPPER.ConfigurationBase hookExtension()
    {    
        AvaTaxConfigManager config = getavaTaxConfig();
        return config;
    }
    
    public AvaTaxConfigManager getavaTaxConfig()
    {
        AvaTax__c avaTaxInstance = AvaTax__c.getInstance(AvaConstants.AVATAX_CUSTOM_SETTING_NAME);
        AvaTaxConfigManager config = new AvaTaxConfigManager();
        AVA_MAPPER.AddressValidationInfo defaultOriginAddress = new AVA_MAPPER.AddressValidationInfo();
        config.Username = avaTaxInstance.Account_ID__c;
        config.Password = avaTaxInstance.License_Key__c;
        config.companyCode = avaTaxInstance.Company_Code__c;
        defaultOriginAddress.country = avaTaxInstance.Country__c;
        config.isTaxCalculationEnabled = avaTaxInstance.Enable_AvaTax_Tax_Calculation__c;
        config.isUPCEnabled = avaTaxInstance.Enable_UPC_Code_as_ItemCode__c;
        defaultOriginAddress.postalcode = avaTaxInstance.Postal_Code__c;
        config.environment = AVA_MAPPER.AvaTaxEnvironment.Production;
        if(avaTaxInstance.Sandbox__c) {
            config.environment = AVA_MAPPER.AvaTaxEnvironment.Sandbox;
        }
        config.saveTransactionToAvaTax = avaTaxInstance.Save_transactions_to_AvaTax__c;
        defaultOriginAddress.city = avaTaxInstance.City__c;
        defaultOriginAddress.line2 = avaTaxInstance.Line2__c;
        defaultOriginAddress.line3 = avaTaxInstance.Line3__c;
        defaultOriginAddress.region = avaTaxInstance.State__c;
        defaultOriginAddress.line1 = avaTaxInstance.Street__c;
        config.isAddressValidationEnabled = avaTaxInstance.Validate_Addresses__c;
        config.defaultOriginAddress = defaultOriginAddress;
        return config;
    }
    
    global AVA_MAPPER.ConfigurationBase getConfig(AVA_MAPPER.CreateTransactionModel getTaxRequest, Map<String,List<SObject>> headerQueryResult, Map<String,List<SObject>> lineQueryResult)
    {
        AvaTax__c avaTaxInstance = AvaTax__c.getInstance(AvaConstants.AVATAX_CUSTOM_SETTING_NAME);
        AvaTaxConfigManager config = new AvaTaxConfigManager();
        config.AccountId = Integer.valueOf(avaTaxInstance.Account_ID__c);
        config.LicenseKey = avaTaxInstance.License_Key__c;
        config.environment = AVA_MAPPER.AvaTaxEnvironment.Production;
        if(avaTaxInstance.Sandbox__c) {
            config.environment = AVA_MAPPER.AvaTaxEnvironment.Sandbox;
        }
        config.isTaxCalculationEnabled = avaTaxInstance.Enable_AvaTax_Tax_Calculation__c;
        return config;
    }
}