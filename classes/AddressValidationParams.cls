public class AddressValidationParams {
    @AuraEnabled public String line1 {get;set;}
    @AuraEnabled public String line2 {get;set;}
    @AuraEnabled public String line3 {get;set;}
    @AuraEnabled public String city {get;set;}
    @AuraEnabled public String region {get;set;}
    @AuraEnabled public String postalCode {get;set;}
    @AuraEnabled public String country {get;set;}
    @AuraEnabled public String avError {get;set;}

}