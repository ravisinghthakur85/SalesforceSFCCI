/**
 * Test suite for MetaDataUtilsTests.
 * 
 * @author Max Rudman
 * @since 8/29/2009
 */
@isTest
private class MetaDataUtilsTests {
    testMethod static void testGetFieldNames() {
        System.assert(MetaDataUtils.getFieldNames('Account').size() > 0);
        System.assert(MetaDataUtils.getFieldNames(Account.sObjectType).size() > 0);
    }
    
    testMethod static void testsGetFields() {
        System.assert(MetaDataUtils.getFieldNames('Account').size() > 0);
    }
    
    testMethod static void testGetDefaultPicklistValue() {
        String value = MetaDataUtils.getDefaultPicklistValue(Account.sObjectType, Account.Type);
        for (Schema.PicklistEntry entry : Account.fields.Type.getDescribe().getPicklistValues()) {
            if (entry.isActive() && entry.isDefaultValue()) {
                System.assertEquals(entry.getValue(), value);
                return;
            }
        }
        System.assertEquals(null, value);
    }
    
    testMethod static void testFindRelationship() {
        System.assertEquals('Contacts', MetaDataUtils.findRelationship(Account.sObjectType, Contact.AccountId).getRelationshipName());
        System.assertEquals('Contacts', MetaDataUtils.findRelationship(Account.sObjectType, Contact.sObjectType, 'AccountId').getRelationshipName());
    }
    
    testMethod static void testGetCalculatedFields() {
        System.assertEquals(0, MetaDataUtils.getCalculatedFields(OpportunityLineItem.sObjectType).size());
    }
    
    testMethod static void testFindNameField() {
        System.assertEquals(Account.Name, MetaDataUtils.findNameField(Account.sObjectType));
        System.assertEquals(Case.CaseNumber, MetaDataUtils.findNameField(Case.sObjectType));
    }
    
    testMethod static void testDetermineObjectType() {
        Account acct = new Account(Name='Test');
        insert acct;
        
        System.assertEquals(Account.sObjectType, MetaDataUtils.determineObjectType(acct.Id));
    }
}