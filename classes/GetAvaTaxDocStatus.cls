Global class GetAvaTaxDocStatus implements Ava_Mapper.IUpdateTaxHelper {
    Global String extension(Ava_Mapper.TransactionModel transResult, Integer lineIndex)
    {
        String docStatus = null;
        if(!String.isBlank(string.valueOf(transResult.status)))
        {
            if(transResult.statusCode == 200 || transResult.statusCode == 201)
            {
                docStatus = string.valueOf(transResult.status);
            }
        }
        if(AvaTax__c.getInstance(AvaConstants.AVATAX_CUSTOM_SETTING_NAME).Save_transactions_to_AvaTax__c == false && OrderTaxCalculator.orderId != null)
        {
            docStatus = null;
        }       
        return docStatus;
    }
}