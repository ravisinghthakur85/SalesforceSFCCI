Global With Sharing class QuoteTaxCalculator
{
    public static DateTime connectorTimeStart;
    public static DateTime connectorTimeRequestFetchStop;
    public static DateTime connectorTimeResponseUpdateStart;
    public static DateTime ConnectorTimeStop;
    public static string logs;
    public static long connectorFetchTime;
    
    Ava_Mapper.TransactionModel transResult;
    public static Id quoteId = null; 
    
    public QuoteTaxCalculator(ApexPages.StandardController controller) {
        quoteId = controller.getRecord().id;
    }
    
    public QuoteTaxCalculator(Id currentQuoteId)
    {
        quoteId = currentQuoteId;
    }
    
    public PageReference calculateTax()
    {
        Map<string, string> taxCalcParam = new Map<string, string>();
        taxCalcParam.put('docId', quoteId);
        //taxCalcParam.put('commitStatus', 'false');
        connectorTimeStart = system.now();
        calculateTax(taxCalcParam);
        return redirectPage();
    }
    
    @InvocableMethod
    public static void calculateTax(List<Id> QuoteId)
    {
        for(ID quoteIdValue : QuoteId)
        {
            Map<string, string> taxCalculationParameter = new Map<string, string>();
            taxCalculationParameter.put('docId', quoteIdValue);
            asyncCalculateTax(taxCalculationParameter);
        }
    }
    
    //This method is used to call from the trigger
    @future (callout = true)
    static public void asyncCalculateTax(map<string, string> taxCalculationParameter)
    {
        connectorTimeStart = system.now();
        calculateTax(taxCalculationParameter);
    }
    
    static public void calculateTax(Map<string, string> taxCalculationParameter)
    {
        string sourceName = '';
        if(system.isFuture())
        {
            sourceName = 'QuoteTaxCalculator.asyncCalculateTax()';
        }
        else
        {
            sourceName = 'QuoteTaxCalculator.CalculateTax()';
        }
        
        try{
            string qtDocId = taxCalculationParameter.get('docId');
            quoteId = qtDocId;
            
            if(String.isBlank(AvaTax__c.getInstance(AvaConstants.AVATAX_CUSTOM_SETTING_NAME).AVA_SFCLOUD__Account_ID__c) || 
              String.isBlank(AvaTax__c.getInstance(AvaConstants.AVATAX_CUSTOM_SETTING_NAME).AVA_SFCLOUD__License_Key__c))
            {
                Throw new SalesCloudAvaTaxException(AvaConstants.AVATAX_CREDENTIALS_ERROR);
            }
            
            if(!AvaTax__c.getInstance(AvaConstants.AVATAX_CUSTOM_SETTING_NAME).Enable_AvaTax_Tax_Calculation__c)
            {
                Throw new SalesCloudAvaTaxException(AvaConstants.TAX_CALCULATION_DISABLED);
            }
            else
            {
                Map<String,String> quoteParam = new Map<String,String>();
                ConfigurationProvider config = new ConfigurationProvider();
                
                string subsidiaryNameLocal='';
                string subsidiaryNameInDBMapping = '';
                //string orgNamespace = '';

                if(String.isNotBlank(AVA_SFCLOUD__AdditionalSettings__c.getValues('AvaTax').CustomFieldApiName__c))
                {
                	//orgNamespace = [SELECT NamespacePrefix FROM Organization LIMIT 1].NamespacePrefix;
                    subsidiaryNameInDBMapping = AVA_SFCLOUD__AdditionalSettings__c.getValues('AvaTax').CustomFieldApiName__c;
                
                    System.debug('subsidiaryNameInDBMapping: '+subsidiaryNameInDBMapping);
                    /*if(!subsidiaryNameInDBMapping.containsIgnoreCase(orgNamespace))
                    {
                        subsidiaryNameInDBMapping = orgNamespace+'__'+subsidiaryNameInDBMapping;
                    }*/

                    if(string.isNotBlank(subsidiaryNameInDBMapping))
                    {
                        subsidiaryNameLocal = 'Account.'+subsidiaryNameInDBMapping;
                    }
                }

                AVA_MAPPER.TaxCalculator quote = new AVA_MAPPER.TaxCalculator(AvaConstants.STATIC_RESOURCE_FILENAME,AvaConstants.AVATAX_PACKAGE_NAMESPACE,config.hookExtension()); 
                //quote.setBeforeTaxValidationListener(new QuoteTaxValidation());
                quote.setCustomBusinessCaseListener(new QuoteTaxCalculatorHelper(subsidiaryNameLocal));
                
                if(String.isBlank(subsidiaryNameInDBMapping))
                {
                    quoteParam.put('subsidiary',null);
                }
                else
                {
                    quoteParam.put('subsidiary',', Quote.Account.' + subsidiaryNameInDBMapping);
                }

                if(UserInfo.isMultiCurrencyOrganization())
                {
                    quoteParam.put('currencyIsoCode',', CurrencyIsoCode');
                }
                
                AVA_MAPPER.TaxCalculationInput taxCalcInput = new AVA_MAPPER.TaxCalculationInput();
                taxCalcInput.recordId = qtDocId;
                taxCalcInput.controller = 'quote';
                taxCalcInput.optionalParams = quoteParam;
                taxCalcInput.isMultiCommit = False; 
                connectorTimeRequestFetchStop = System.now();
                connectorFetchTime = connectorTimeRequestFetchStop.getTime() - connectorTimeStart.getTime();
                
                Ava_Mapper.TransactionModel transResult = quote.calculateTax(taxCalcInput);
                connectorTimeResponseUpdateStart = System.now();
                if(transResult.statusCode == 200 || transResult.statusCode == 201)
                {
                    ConnectorTimeStop = System.now();
                    long connectorUpdateTime = ConnectorTimeStop.getTime() - ConnectorTimeResponseUpdateStart.getTime();
                    long connectorTime = connectorUpdateTime +connectorFetchTime;
                    logs = UtilityHelper.CreateGetTaxLogs(transResult, 'calculateTax()',string.valueof(connectorTime), sourceName,'CreateTransaction','GetTax', null);
                    system.enqueueJob(new AVA_MAPPER.QueuableLogger(logs,UtilityHelper.getLogEntriesToken()));
                }
                else if(transResult.statusCode == 501)
                {
                    logs = UtilityHelper.CreateGetTaxLogs(transResult, 'calculateTax()',null, sourceName,null,null, transResult.error.message);
                    logs = UtilityHelper.trimNewLineCharacterFromString(logs);
                    system.enqueueJob(new AVA_MAPPER.QueuableLogger(logs,UtilityHelper.getLogEntriesToken()));
                }
            }
        }
        catch(Exception e)
        {
            logs = UtilityHelper.CreateGetTaxLogs(null, 'calculateTax()',null, sourceName,null,null, string.valueOf(e.getMessage()+','+e.getStackTraceString()));
            logs = UtilityHelper.trimNewLineCharacterFromString(logs);
            system.enqueueJob(new AVA_MAPPER.QueuableLogger(logs,UtilityHelper.getLogEntriesToken()));
            
            Quote updateQuote = new Quote(Id=quoteId);
            if (Schema.sObjectType.Quote.fields.AvaTax_Message__c.isUpdateable()){
                updateQuote.AvaTax_Message__c = e.getMessage()+','+e.getStackTraceString();
                update updateQuote;
            }
        }
    }
    
    public PageReference redirectPage() {
        // Redirect the user back to the original page         
        PageReference pageRef = new PageReference('/' + quoteId);
        pageRef.setRedirect(true);
        return pageRef;
    }
}