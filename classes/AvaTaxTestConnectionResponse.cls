global class AvaTaxTestConnectionResponse
{
    @AuraEnabled public string tcResponse{get;set;}
    @AuraEnabled public string defaultValue{get;set;}
    @AuraEnabled public List<Shipping_Code__c> shippingCode{get;set;}
    @AuraEnabled public List<AvaTaxCompany__c> optionList{get;set;}
    @AuraEnabled public AvaTax__c configurationList{get;set;}

    @AuraEnabled public Integer pageSize {get;set;}
    @AuraEnabled public Integer page {get;set;}
    @AuraEnabled public Integer total {get;set;}
    @AuraEnabled public List<MultiCompanyModel> lstMultiCompanyMapping {get;set;}
    @AuraEnabled  public List<Countries__c> countryListing {get; set;}

    @AuraEnabled  public String cautionString {get; set;}
    @AuraEnabled  public String customObjName {get; set;}
    @AuraEnabled  public Boolean displayMultiMapping {get; set;}
    @AuraEnabled  public String customFieldName {get; set;}
    @AuraEnabled  public String responseString {get; set;}    

    @AuraEnabled  public AvaTax__c avaTaxConfiguration {get; set;}

}