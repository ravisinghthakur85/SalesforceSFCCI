@isTest
private class TestClassQuote {
    private static AvaTax__c avaTaxInstance = null;
    private static Account testAccount = null;
    private static Contact testContact = null;
    private static Opportunity testOpp = null;
    
    //@testSetup
    private testMethod static void testQuote()
    {
        AvaTax__c avaTaxInstance = new AvaTax__c(
            Name = 'AvaTax Config',
            Account_ID__c = 'test',
            License_Key__c = 'test',
            Company_Code__c = 'default',
            Country__c = 'US',
            City__c = 'Seattle',
            Enable_AvaTax_Tax_Calculation__c = true,
            Enable_UPC_Code_as_ItemCode__c = true,
            Postal_Code__c = '98110',
            Sandbox__c = true,
            Save_transactions_to_AvaTax__c = true,
            State__c = 'WA',
            Street__c = '900 winslow way e',
            Validate_Addresses__c = true,
            AVA_SFCLOUD__Shipping_Code__c = 'FR',
            AVA_SFCLOUD__Allow_Tax_Override__c = true);
        insert avaTaxInstance;
        
        AdditionalSettings__c settings = new AdditionalSettings__c(Name='AvaTax');
        insert settings;
        
        Account testAccount = new Account(Name=DateTime.now().format(), BillingStreet = '900 winlsow way e', BillingCity = 'BI', BillingState='WA', BillingPostalCode = '98110', 
                                          BillingCountry = 'US',ShippingStreet = '', ShippingCity = '', ShippingState='', ShippingPostalCode = '', 
                                          ShippingCountry = '',AVA_MAPPER__Business_Identification_Number__c = '1234');
        insert testAccount;
        
        Contact testContact = new Contact(LastName=DateTime.now().format(), AccountId=testAccount.Id,
                                          MailingStreet = '900 winslow way e', MailingCity = 'BI', MailingState='WA', MailingPostalCode = '98110-2450', MailingCountry = 'US',
                                          OtherStreet = '900 winslow way e', OtherCity = 'BI', OtherState='WA', OtherPostalCode = '98110-2450', OtherCountry = 'US');
        
        insert testContact;
        
        Opportunity testOpp = new Opportunity(Name=DateTime.now().format(),StageName='xx',CloseDate=Date.today().addDays(30),AccountId=testAccount.Id, 
                                              AvaTax_Doc_Status__c = null, AvaTax_Message__c = null, Invoice_Message__c = null, 
                                              Sales_Tax__c = null, Tax_Date__c=null);
        insert testOpp;
        
        
        PriceBook2 pricebookTest = new Pricebook2( Name ='Test PriceBook',
                                                  isActive = true                                               
                                                 );
        insert pricebookTest;
        
        Product2 productTest = new Product2( Name ='Test Product',
                                            Family = 'Hardware',
                                            ProductCode = 'testcode',
                                            Description = 'XYZ' 
                                           );
        insert productTest;
        
        Id standardPriceBookId = Test.getStandardPricebookId();
        PriceBookEntry pricebookEntryTest = new PricebookEntry( UnitPrice = 500,
                                                               Pricebook2Id = standardPriceBookId,
                                                               Product2Id = productTest.Id,
                                                               isActive = true                                                
                                                              );
        insert pricebookEntryTest;
        
        testOpp.Sales_Tax__c = 100.00;
        testOpp.tax_date__c = System.today() - 1;
        //testOpp.Opp_Entity_Use_Code__c = EntityUse__c.PicklistField__c.getDescribe().getPicklistValues()[0].getValue();
        //testOpp.Opp_Entity_Use_Code__c = SObjectType.EntityUse__c.Fields.PicklistValues[0].getValue(); 
        update testOpp;
        
        OpportunityLineItem testLine = new OpportunityLineItem(Description='xx',OpportunityId=testOpp.Id,
                                                               PricebookEntryId=pricebookEntryTest.Id, Quantity=1.0,TotalPrice=100.00,
                                                               SalesTax_Line__c=null,Sales_Tax_Details__c='xx\r\n\r\nxx',Sales_Tax_Rate__c='10.00%');
        insert testLine;
        
        Quote testQuote = new Quote(Name=DateTime.now().format(), OpportunityId=testOpp.Id,ShippingStreet = '123 anystreet', AVA_SFCLOUD__AvaTax_Doc_Status__c=null,
                                    AVA_SFCLOUD__Tax_Date__c=null,ShippingCity = null, ShippingState = null,BillingCity = 'BI',BillingPostalCode = '98110',BillingCountry = 'US', 
                                    BillingStreet = '900 winslow way e/waye', BillingState = 'WA',AVA_SFCLOUD__AvaTax_Message__c=null,ShippingPostalCode=null,ShippingHandling=100, pricebook2id=standardPriceBookId);
        //testQuote.Pricebook2Id = testpb[0].Id;
        insert testQuote;
        testQuote.AVA_SFCLOUD__Is_Seller_Importer_Of_Record__c = true;
        testQuote.AVA_SFCLOUD__Tax_Date__c = DateTime.Now().AddDays(2);
        update testQuote;
        
        QuoteLineItem testQuoteLine = new QuoteLineItem(Description='xx',QuoteId=testQuote.Id,
                                                        PricebookEntryId=pricebookEntryTest.Id, AVA_SFCLOUD__SalesTax_Line__c=null,
                                                        AVA_SFCLOUD__Sales_Tax_Details__c='xx\r\n\r\nxx',AVA_SFCLOUD__Sales_Tax_Rate__c='10.00%',AVA_SFCLOUD__Tax_Override__c=true, Quantity=1.0, UnitPrice=12.0);
        insert testQuoteLine;
        
        ConfigurationProvider config = new ConfigurationProvider();
        AVA_MAPPER.TaxCalculator quote = new AVA_MAPPER.TaxCalculator('SFConfig','AVA_SFCLOUD',config.hookExtension());
        
        PageReference pageRef = Page.Quote_Calculate_Tax;
        Test.setCurrentPage(pageRef);
        pageRef.getParameters().put('id',testQuote.Id);
        ApexPages.StandardController sc = new ApexPages.standardController(testQuote);
        QuoteTaxCalculator gtQuote = new QuoteTaxCalculator(sc);
        gtQuote.CalculateTax();
        
        QuoteTaxCalculatorHelper gtQuoteHelper = new QuoteTaxCalculatorHelper('1234');

        List<Id> listQuoteId = new List<Id>();
        listQuoteId.add(testQuote.Id);

        system.assertEquals(testQuote.AVA_SFCLOUD__AvaTax_Message__c,null);
        
        QuoteTaxCalculator.calculateTax(listQuoteId);        
        QuoteTaxCalculator qtTaxCalc = new QuoteTaxCalculator(testQuote.Id);
        system.assertNotEquals(qtTaxCalc, null);
    }
    
    private testMethod static void testQuoteMultiEntity()
    {
        AvaTax__c avaTaxInstance = new AvaTax__c(
            Name = 'AvaTax Config',
            Account_ID__c = 'test',
            License_Key__c = 'test',
            Company_Code__c = 'default',
            Country__c = 'US',
            City__c = 'Seattle',
            Enable_AvaTax_Tax_Calculation__c = true,
            Enable_UPC_Code_as_ItemCode__c = true,
            Postal_Code__c = '98110',
            Sandbox__c = true,
            Save_transactions_to_AvaTax__c = true,
            State__c = 'WA',
            Street__c = '900 winslow way e',
            Validate_Addresses__c = true,
            Allow_Tax_Override__c = true);
        insert avaTaxInstance;
        
        //Subsidiary__c subsidiary = new Subsidiary__c(Name = 'A1');
        //insert subsidiary;
        
        AdditionalSettings__c settings = new AdditionalSettings__c(Name='AvaTax', 
                                                                   CustomObjectApiName__c = 'Subsidiary__c',
                                                                  CustomFieldApiName__c = 'Subsidiary_Name__c');
        insert settings;
        
        Account testAccount = new Account(Name=DateTime.now().format(), BillingStreet = '900 winlsow way e', BillingCity = 'BI', BillingState='WA', BillingPostalCode = '98110', 
                                          BillingCountry = 'US',ShippingStreet = '', ShippingCity = '', ShippingState='', ShippingPostalCode = '', 
                                          ShippingCountry = '',AVA_MAPPER__Business_Identification_Number__c = '1234');
        insert testAccount;
        
        Contact testContact = new Contact(LastName=DateTime.now().format(), AccountId=testAccount.Id,
                                          MailingStreet = '900 winslow way e', MailingCity = 'BI', MailingState='WA', MailingPostalCode = '98110-2450', MailingCountry = 'US',
                                          OtherStreet = '900 winslow way e', OtherCity = 'BI', OtherState='WA', OtherPostalCode = '98110-2450', OtherCountry = 'US');
        
        insert testContact;
        
        Opportunity testOpp = new Opportunity(Name=DateTime.now().format(),StageName='xx',CloseDate=Date.today().addDays(30),AccountId=testAccount.Id, 
                                              AvaTax_Doc_Status__c = null, AvaTax_Message__c = null, Invoice_Message__c = null, 
                                              Sales_Tax__c = null, Tax_Date__c=null);
        insert testOpp;
        
        
        PriceBook2 pricebookTest = new Pricebook2( Name ='Test PriceBook',
                                                  isActive = true                                               
                                                 );
        insert pricebookTest;
        
        Product2 productTest = new Product2( Name ='Test Product',
                                            Family = 'Hardware',
                                            UPC__c = 'abc',
                                            Description = 'XYZ' 
                                           );
        insert productTest;
        
        Id standardPriceBookId = Test.getStandardPricebookId();
        PriceBookEntry pricebookEntryTest = new PricebookEntry( UnitPrice = 500,
                                                               Pricebook2Id = standardPriceBookId,
                                                               Product2Id = productTest.Id,
                                                               isActive = true                                                
                                                              );
        insert pricebookEntryTest;
        
        testOpp.Sales_Tax__c = 100.00;
        update testOpp;
        
        OpportunityLineItem testLine = new OpportunityLineItem(Description='xx',OpportunityId=testOpp.Id,
                                                               PricebookEntryId=pricebookEntryTest.Id, Quantity=1.0,TotalPrice=100.00,
                                                               SalesTax_Line__c=null,Sales_Tax_Details__c='xx\r\n\r\nxx',Sales_Tax_Rate__c='10.00%');
        insert testLine;
        
        Quote testQuote = new Quote(Name=DateTime.now().format(), OpportunityId=testOpp.Id,ShippingStreet = '123 anystreet', AVA_SFCLOUD__AvaTax_Doc_Status__c=null,
                                    AVA_SFCLOUD__Tax_Date__c=null,ShippingCity = null, ShippingState = null,BillingCity = 'BI',BillingPostalCode = '98110',BillingCountry = 'US', 
                                    BillingStreet = '900 winslow way e/waye', BillingState = 'WA',AVA_SFCLOUD__AvaTax_Message__c=null,ShippingPostalCode=null,ShippingHandling=100, pricebook2id=standardPriceBookId);
        //testQuote.Pricebook2Id = testpb[0].Id;
        insert testQuote;
        testQuote.AVA_SFCLOUD__Is_Seller_Importer_Of_Record__c = true;
        testQuote.AVA_SFCLOUD__Tax_Date__c = DateTime.Now().AddDays(2);
        update testQuote;
        
        QuoteLineItem testQuoteLine = new QuoteLineItem(Description='xx',QuoteId=testQuote.Id,
                                                        PricebookEntryId=pricebookEntryTest.Id, AVA_SFCLOUD__SalesTax_Line__c=null,
                                                        AVA_SFCLOUD__Sales_Tax_Details__c='xx\r\n\r\nxx',AVA_SFCLOUD__Sales_Tax_Rate__c='10.00%',AVA_SFCLOUD__Tax_Override__c=true, Quantity=1.0, UnitPrice=12.0);
        insert testQuoteLine;
        
        QuoteLineItem testLineToUpdate = [SELECT id, AVA_SFCLOUD__Tax_Override__c, AVA_SFCLOUD__SalesTax_Line__c FROM QuoteLineItem WHERE id = : testQuoteLine.id];
        testLineToUpdate.AVA_SFCLOUD__Tax_Override__c = true;
        testLineToUpdate.AVA_SFCLOUD__SalesTax_Line__c = 10;
        update testLineToUpdate;
        
        ConfigurationProvider config = new ConfigurationProvider();
        AVA_MAPPER.TaxCalculator quote = new AVA_MAPPER.TaxCalculator('SFConfig','AVA_SFCLOUD',config.hookExtension());
        
        PageReference pageRef = Page.Quote_Calculate_Tax;
        Test.setCurrentPage(pageRef);
        pageRef.getParameters().put('id',testQuote.Id);
        ApexPages.StandardController sc = new ApexPages.standardController(testQuote);
        QuoteTaxCalculator gtQuote = new QuoteTaxCalculator(sc);
        gtQuote.CalculateTax();
        
        QuoteTaxCalculatorHelper gtQuoteHelper = new QuoteTaxCalculatorHelper('1234');

        List<Id> listQuoteId = new List<Id>();
        listQuoteId.add(testQuote.Id);

        system.assertEquals(testQuote.AVA_SFCLOUD__AvaTax_Message__c,null);
        
        QuoteTaxCalculator.calculateTax(listQuoteId);        
        QuoteTaxCalculator qtTaxCalc = new QuoteTaxCalculator(testQuote.Id);
        system.assertNotEquals(qtTaxCalc, null);
    }
    
    @isTest
    private static void executeMethodExceptionTest(){
		QuoteTaxCalculatorHelper qq = new QuoteTaxCalculatorHelper('Name');
        try{
            qq.execute(null,null,null);
        }catch(Exception ex){
            system.debug('ex:'+ex.getMessage() + ex.getStackTraceString());
        }
    }
    
    @isTest
    private static void executeMethodTest(){
		QuoteTaxCalculatorHelper qqq = new QuoteTaxCalculatorHelper('');
        Map <String, List <SObject>> hResult = new Map <String, List <SObject>>();
        Map <String, List <SObject>> lResult = new Map <String, List <SObject>>();
        
        Account testAccount = new Account(Name=DateTime.now().format(), BillingStreet = '900 winlsow way e', BillingCity = 'BI', BillingState='WA', BillingPostalCode = '98110', 
                                          BillingCountry = 'US',ShippingStreet = '', ShippingCity = '', ShippingState='', ShippingPostalCode = '', 
                                          ShippingCountry = '',AVA_MAPPER__Business_Identification_Number__c = '1234');
        insert testAccount;
        
        Contact testContact = new Contact(LastName=DateTime.now().format(), AccountId=testAccount.Id,
                                          MailingStreet = '900 winslow way e', MailingCity = 'BI', MailingState='WA', MailingPostalCode = '98110-2450', MailingCountry = 'US',
                                          OtherStreet = '900 winslow way e', OtherCity = 'BI', OtherState='WA', OtherPostalCode = '98110-2450', OtherCountry = 'US');
        
        insert testContact;
        
        Opportunity testOpp = new Opportunity(Name=DateTime.now().format(),StageName='xx',CloseDate=Date.today().addDays(30),AccountId=testAccount.Id, 
                                              AvaTax_Doc_Status__c = null, AvaTax_Message__c = null, Invoice_Message__c = null, 
                                              Sales_Tax__c = null, Tax_Date__c=null);
        insert testOpp;
        
        
        PriceBook2 pricebookTest = new Pricebook2( Name ='Test PriceBook',
                                                  isActive = true                                               
                                                 );
        insert pricebookTest;
        
        Product2 productTest = new Product2( Name ='Test Product',
                                            Family = 'Hardware',
                                            Description = 'XYZ',
                                            UPC__c = 'abc'
                                           );
        insert productTest;
        
        Id standardPriceBookId = Test.getStandardPricebookId();
        PriceBookEntry pricebookEntryTest = new PricebookEntry( UnitPrice = 500,
                                                               Pricebook2Id = standardPriceBookId,
                                                               Product2Id = productTest.Id,
                                                               isActive = true                                                
                                                              );
        insert pricebookEntryTest;
        
        OpportunityLineItem testLine = new OpportunityLineItem(Description='xx',OpportunityId=testOpp.Id,
            PricebookEntryId=pricebookEntryTest.Id, Quantity=1.0,TotalPrice=100.00);
            
        insert testLine;
		
        Quote testQuote = new Quote(Name=DateTime.now().format(), OpportunityId=testOpp.Id,ShippingStreet = '123 anystreet', AVA_SFCLOUD__AvaTax_Doc_Status__c=null,
                                    AVA_SFCLOUD__Tax_Date__c=null,ShippingCity = null, ShippingState = null,BillingCity = 'BI',BillingPostalCode = '98110',BillingCountry = 'US', 
                                    BillingStreet = '900 winslow way e/waye', BillingState = 'WA',AVA_SFCLOUD__AvaTax_Message__c=null,ShippingPostalCode=null,ShippingHandling=100, pricebook2id=standardPriceBookId);

        insert testQuote;
        
        QuoteLineItem testQuoteLine = new QuoteLineItem(Description='xx',QuoteId=testQuote.Id,
                                                        PricebookEntryId=pricebookEntryTest.Id, AVA_SFCLOUD__SalesTax_Line__c=null,
                                                        AVA_SFCLOUD__Sales_Tax_Details__c='xx\r\n\r\nxx',AVA_SFCLOUD__Sales_Tax_Rate__c='10.00%',AVA_SFCLOUD__Tax_Override__c=true, Quantity=1.0, UnitPrice=12.0);
        insert testQuoteLine;
        
        hResult.put('quote',new List<SObject>{testQuote});
        lResult.put('quotelineitem',new List<SObject>{testQuoteLine});
        
        AVA_MAPPER.CreateTransactionModel model = new AVA_MAPPER.CreateTransactionModel();

        try{
        qqq.execute(model,hResult,lResult);
        }
        catch(Exception ex){
            System.debug('In exception');
        }
    }
    
    @isTest
    private static void executeMethodTestCoverage(){
		QuoteTaxCalculatorHelper qqq = new QuoteTaxCalculatorHelper('');
        Map <String, List <SObject>> hResult = new Map <String, List <SObject>>();
        Map <String, List <SObject>> lResult = new Map <String, List <SObject>>();
        
        Account testAccount = new Account(Name=DateTime.now().format(), BillingStreet = '900 winlsow way e', BillingCity = 'BI', BillingState='WA', BillingPostalCode = '98110', 
                                          BillingCountry = 'US',ShippingStreet = '', ShippingCity = '', ShippingState='', ShippingPostalCode = '', 
                                          ShippingCountry = '',AVA_MAPPER__Business_Identification_Number__c = '1234');
        insert testAccount;
        
        Contact testContact = new Contact(LastName=DateTime.now().format(), AccountId=testAccount.Id,
                                          MailingStreet = '900 winslow way e', MailingCity = 'BI', MailingState='WA', MailingPostalCode = '98110-2450', MailingCountry = 'US',
                                          OtherStreet = '900 winslow way e', OtherCity = 'BI', OtherState='WA', OtherPostalCode = '98110-2450', OtherCountry = 'US');
        
        insert testContact;
        
        Opportunity testOpp = new Opportunity(Name=DateTime.now().format(),StageName='xx',CloseDate=Date.today().addDays(30),AccountId=testAccount.Id, 
                                              AvaTax_Doc_Status__c = null, AvaTax_Message__c = null, Invoice_Message__c = null, 
                                              Sales_Tax__c = null, Tax_Date__c=null);
        insert testOpp;
        
        
        PriceBook2 pricebookTest = new Pricebook2( Name ='Test PriceBook',
                                                  isActive = true                                               
                                                 );
        insert pricebookTest;
        
        Product2 productTest = new Product2( Name ='Test Product',
                                            Family = 'Hardware',
                                            Description = 'XYZ',
                                            UPC__c = 'abc'
                                           );
        insert productTest;
        
        Id standardPriceBookId = Test.getStandardPricebookId();
        PriceBookEntry pricebookEntryTest = new PricebookEntry( UnitPrice = 500,
                                                               Pricebook2Id = standardPriceBookId,
                                                               Product2Id = productTest.Id,
                                                               isActive = true                                                
                                                              );
        insert pricebookEntryTest;
        
        OpportunityLineItem testLine = new OpportunityLineItem(Description='xx',OpportunityId=testOpp.Id,
            PricebookEntryId=pricebookEntryTest.Id, Quantity=1.0,TotalPrice=100.00);
            
        insert testLine;
		
        Quote testQuote = new Quote(Name=DateTime.now().format(), OpportunityId=testOpp.Id,ShippingStreet = null, AVA_SFCLOUD__AvaTax_Doc_Status__c=null,
                                    AVA_SFCLOUD__Tax_Date__c=null,ShippingCity = null, ShippingState = null,BillingCity = 'BI',BillingPostalCode = '98110',BillingCountry = 'US', 
                                    BillingStreet = '900 winslow way e/waye', BillingState = 'WA',AVA_SFCLOUD__AvaTax_Message__c=null,ShippingPostalCode=null,ShippingHandling=100, pricebook2id=standardPriceBookId);

        insert testQuote;
        
        QuoteLineItem testQuoteLine = new QuoteLineItem(Description='xx',QuoteId=testQuote.Id,
                                                        PricebookEntryId=pricebookEntryTest.Id, AVA_SFCLOUD__SalesTax_Line__c=null,
                                                        AVA_SFCLOUD__Sales_Tax_Details__c='xx\r\n\r\nxx',AVA_SFCLOUD__Sales_Tax_Rate__c='10.00%',AVA_SFCLOUD__Tax_Override__c=true, Quantity=1.0, UnitPrice=12.0);
        insert testQuoteLine;
        
        hResult.put('quote',new List<SObject>{testQuote});
        lResult.put('quotelineitem',new List<SObject>{testQuoteLine});
        
        AVA_MAPPER.CreateTransactionModel model = new AVA_MAPPER.CreateTransactionModel();

        try{
        qqq.execute(model,hResult,lResult);
        }
        catch(Exception ex){
            System.debug('In exception');            
        }
    }
    
    private testMethod static void testQuoteDisableTax()
    {
        AvaTax__c avaTaxInstance = new AvaTax__c(
            Name = 'AvaTax Config',
            Account_ID__c = 'test',
            License_Key__c = 'test',
            Company_Code__c = 'default',
            Country__c = 'US',
            City__c = 'Seattle',
            Enable_AvaTax_Tax_Calculation__c = false,
            Enable_UPC_Code_as_ItemCode__c = true,
            Postal_Code__c = '98110',
            Sandbox__c = true,
            Save_transactions_to_AvaTax__c = true,
            State__c = 'WA',
            Street__c = '900 winslow way e',
            Validate_Addresses__c = true,
            AVA_SFCLOUD__Allow_Tax_Override__c = true);
        insert avaTaxInstance;
        
        AdditionalSettings__c settings = new AdditionalSettings__c(Name='AvaTax');
        insert settings;
        
        Account testAccount = new Account(Name=DateTime.now().format(), BillingStreet = '900 winlsow way e', BillingCity = 'BI', BillingState='WA', BillingPostalCode = '98110', 
                                          BillingCountry = 'US',ShippingStreet = '', ShippingCity = '', ShippingState='', ShippingPostalCode = '', 
                                          ShippingCountry = '',AVA_MAPPER__Business_Identification_Number__c = '1234');
        insert testAccount;
        
        Contact testContact = new Contact(LastName=DateTime.now().format(), AccountId=testAccount.Id,
                                          MailingStreet = '900 winslow way e', MailingCity = 'BI', MailingState='WA', MailingPostalCode = '98110-2450', MailingCountry = 'US',
                                          OtherStreet = '900 winslow way e', OtherCity = 'BI', OtherState='WA', OtherPostalCode = '98110-2450', OtherCountry = 'US');
        
        insert testContact;
        
        Opportunity testOpp = new Opportunity(Name=DateTime.now().format(),StageName='xx',CloseDate=Date.today().addDays(30),AccountId=testAccount.Id, 
                                              AvaTax_Doc_Status__c = null, AvaTax_Message__c = null, Invoice_Message__c = null, 
                                              Sales_Tax__c = null, Tax_Date__c=null);
        insert testOpp;
        
        
        PriceBook2 pricebookTest = new Pricebook2( Name ='Test PriceBook',
                                                  isActive = true                                               
                                                 );
        insert pricebookTest;
        
        Product2 productTest = new Product2( Name ='Test Product',
                                            Family = 'Hardware',
                                            ProductCode = 'testcode',
                                            Description = 'XYZ' 
                                           );
        insert productTest;
        
        Id standardPriceBookId = Test.getStandardPricebookId();
        PriceBookEntry pricebookEntryTest = new PricebookEntry( UnitPrice = 500,
                                                               Pricebook2Id = standardPriceBookId,
                                                               Product2Id = productTest.Id,
                                                               isActive = true                                                
                                                              );
        insert pricebookEntryTest;
        
        testOpp.Sales_Tax__c = 100.00;
        testOpp.tax_date__c = System.today() - 1;
        //testOpp.Opp_Entity_Use_Code__c = EntityUse__c.PicklistField__c.getDescribe().getPicklistValues()[0].getValue();
        //testOpp.Opp_Entity_Use_Code__c = SObjectType.EntityUse__c.Fields.PicklistValues[0].getValue(); 
        update testOpp;
        
        OpportunityLineItem testLine = new OpportunityLineItem(Description='xx',OpportunityId=testOpp.Id,
                                                               PricebookEntryId=pricebookEntryTest.Id, Quantity=1.0,TotalPrice=100.00,
                                                               SalesTax_Line__c=null,Sales_Tax_Details__c='xx\r\n\r\nxx',Sales_Tax_Rate__c='10.00%');
        insert testLine;
        
        Quote testQuote = new Quote(Name=DateTime.now().format(), OpportunityId=testOpp.Id,ShippingStreet = '123 anystreet', AVA_SFCLOUD__AvaTax_Doc_Status__c=null,
                                    AVA_SFCLOUD__Tax_Date__c=null,ShippingCity = null, ShippingState = null,BillingCity = 'BI',BillingPostalCode = '98110',BillingCountry = 'US', 
                                    BillingStreet = '900 winslow way e/waye', BillingState = 'WA',AVA_SFCLOUD__AvaTax_Message__c=null,ShippingPostalCode=null,ShippingHandling=100, pricebook2id=standardPriceBookId);
        //testQuote.Pricebook2Id = testpb[0].Id;
        insert testQuote;
        testQuote.AVA_SFCLOUD__Is_Seller_Importer_Of_Record__c = true;
        testQuote.AVA_SFCLOUD__Tax_Date__c = DateTime.Now().AddDays(2);
        update testQuote;
        
        QuoteLineItem testQuoteLine = new QuoteLineItem(Description='xx',QuoteId=testQuote.Id,
                                                        PricebookEntryId=pricebookEntryTest.Id, AVA_SFCLOUD__SalesTax_Line__c=null,
                                                        AVA_SFCLOUD__Sales_Tax_Details__c='xx\r\n\r\nxx',AVA_SFCLOUD__Sales_Tax_Rate__c='10.00%',AVA_SFCLOUD__Tax_Override__c=true, Quantity=1.0, UnitPrice=12.0);
        insert testQuoteLine;
        
        ConfigurationProvider config = new ConfigurationProvider();
        AVA_MAPPER.TaxCalculator quote = new AVA_MAPPER.TaxCalculator('SFConfig','AVA_SFCLOUD',config.hookExtension());
        
        PageReference pageRef = Page.Quote_Calculate_Tax;
        Test.setCurrentPage(pageRef);
        pageRef.getParameters().put('id',testQuote.Id);
        ApexPages.StandardController sc = new ApexPages.standardController(testQuote);
        QuoteTaxCalculator gtQuote = new QuoteTaxCalculator(sc);
        gtQuote.CalculateTax();
        
        QuoteTaxCalculatorHelper gtQuoteHelper = new QuoteTaxCalculatorHelper('1234');

        List<Id> listQuoteId = new List<Id>();
        listQuoteId.add(testQuote.Id);

        system.assertEquals(testQuote.AVA_SFCLOUD__AvaTax_Message__c,null);
        
        QuoteTaxCalculator.calculateTax(listQuoteId);        
        QuoteTaxCalculator qtTaxCalc = new QuoteTaxCalculator(testQuote.Id);
        system.assertNotEquals(qtTaxCalc, null);
    }
}