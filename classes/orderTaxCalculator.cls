Global With Sharing class orderTaxCalculator
{
    //Ava_Mapper.TransactionModel transResult;
    public static Id orderId = null; 
    public List<Order> loOrder = null;
    public string docStatus = null;
    public static DateTime connectorTimeStart;
    public static DateTime connectorTimeRequestFetchStop;
    public static DateTime connectorTimeResponseUpdateStart;
    public static DateTime connectorTimeStop;
    public static string logs;
    public static long connectorFetchTime;
    
    public orderTaxCalculator(ApexPages.StandardController controller) 
    {
        orderId = controller.getRecord().id;
        if (Schema.sObjectType.Order.isAccessible())	{
        	loOrder = [select Id, AVA_SFCLOUD__AvaTax_Doc_Status__c FROM Order where id =:orderId];
        	docStatus = loOrder[0].AVA_SFCLOUD__AvaTax_Doc_Status__c;
        }
    }
    
    public OrderTaxCalculator(Id currentorderId)
    {
        orderId = currentOrderId;
    }
    
    public PageReference calculateTax()
    {
        Map<string, string> taxCalcParam = new Map<string, string>();
        taxCalcParam.put('docId', orderId);
        taxCalcParam.put('docStatus', docStatus);
        taxCalcParam.put('commitStatus', 'False');
        connectorTimeStart = system.now();
        calculateTax(taxCalcParam);
        return redirectPage();
    }
    
    @InvocableMethod
    public static void calculateTax(List<ProcessBuilderInput> orderFieldValues)
    {
        //taxCalculationParameter.put('docId', OrderId[0]);
        for(processBuilderInput orderFieldValue : orderFieldValues)
        {
            Map<string, string> taxCalculationParameter = new Map<string, string>();
            taxCalculationParameter.put('docId', orderFieldValue.docId);
            taxCalculationParameter.put('docStatus', orderFieldValue.docStatus);
            taxCalculationParameter.put('commitStatus', orderFieldValue.commitStatus);
            asyncCalculateTax(taxCalculationParameter);
        }
    }
    
    //This method is used to call from the trigger
    @future (callout = true)
    static public void asyncCalculateTax(map<string, string> taxCalculationParameter)
    {
        connectorTimeStart = system.now();
        calculateTax(taxCalculationParameter);
    }
    
    static public void calculateTax(Map<string, string> taxCalculationParameter)
    {
        string sourceName = '';
        string operation = '';
        string type = '';
        
        if(system.isFuture())
        {
            sourceName = 'OrderTaxCalculator.asyncCalculateTax()';
        }
        else
        {
            sourceName = 'OrderTaxCalculator.CalculateTax()';
        }
        
        try
        { 
            string orderDocId = taxCalculationParameter.get('docId');
            orderId = orderDocId;
            string orderDocStatus = taxCalculationParameter.get('docStatus');
            boolean orderCommitStatus = Boolean.valueOf(taxCalculationParameter.get('commitStatus'));
            
            if(String.isBlank(AvaTax__c.getInstance(AvaConstants.AVATAX_CUSTOM_SETTING_NAME).AVA_SFCLOUD__Account_ID__c) || 
              String.isBlank(AvaTax__c.getInstance(AvaConstants.AVATAX_CUSTOM_SETTING_NAME).AVA_SFCLOUD__License_Key__c))
            {
                Throw new SalesCloudAvaTaxException(AvaConstants.AVATAX_CREDENTIALS_ERROR);
            }

            if(!AvaTax__c.getInstance(AvaConstants.AVATAX_CUSTOM_SETTING_NAME).Enable_AvaTax_Tax_Calculation__c)
            {
                Throw new SalesCloudAvaTaxException(AvaConstants.TAX_CALCULATION_DISABLED);
            }
            else
            {
                Map<String,String> orderParam = new Map<String,String>();
                ConfigurationProvider config = new ConfigurationProvider();

                string subsidiaryNameLocal='';
                string subsidiaryNameInDBMapping = '';
                //string orgNamespace = '';

                if(String.isNotBlank(AVA_SFCLOUD__AdditionalSettings__c.getValues('AvaTax').CustomFieldApiName__c))
                {
                	//orgNamespace = [SELECT NamespacePrefix FROM Organization LIMIT 1].NamespacePrefix;
                    subsidiaryNameInDBMapping = AVA_SFCLOUD__AdditionalSettings__c.getValues('AvaTax').CustomFieldApiName__c;
                
                    System.debug('subsidiaryNameInDBMapping: '+subsidiaryNameInDBMapping);
                    /*if(!subsidiaryNameInDBMapping.containsIgnoreCase(orgNamespace))
                    {
                        subsidiaryNameInDBMapping = orgNamespace+'__'+subsidiaryNameInDBMapping;
                    }*/

                    if(string.isNotBlank(subsidiaryNameInDBMapping))
                    {
                        subsidiaryNameLocal = 'Account.'+subsidiaryNameInDBMapping;
                    }
                }

                AVA_MAPPER.TaxCalculator order = new AVA_MAPPER.TaxCalculator(AvaConstants.STATIC_RESOURCE_FILENAME,AvaConstants.AVATAX_PACKAGE_NAMESPACE,config.hookExtension()); 
                //order.setBeforeTaxValidationListener(new OrderTaxValidation());
                
                System.debug('subsidiaryNameInDBMapping: '+subsidiaryNameInDBMapping);
                order.setCustomBusinessCaseListener(new OrderTaxCalculatorHelper(orderCommitStatus,subsidiaryNameLocal));

                if(String.isBlank(subsidiaryNameInDBMapping))
                {
                    orderParam.put('subsidiary',null);
                }
                else
                {
                    orderParam.put('subsidiary',', Order.Account.' + subsidiaryNameInDBMapping);
                }

                if(UserInfo.isMultiCurrencyOrganization())
                {
                    orderParam.put('currencyIsoCode',', CurrencyIsoCode');
                }
                
                AVA_MAPPER.TaxCalculationInput taxCalcInput = new AVA_MAPPER.TaxCalculationInput();

                System.debug('orderParam: '+orderParam);
                taxCalcInput.recordId = orderDocId;
                taxCalcInput.controller = 'order';
                taxCalcInput.optionalParams = orderParam;
                taxCalcInput.isMultiCommit = (orderDocStatus == 'Committed') ? True: False; 
                if(taxCalcInput.isMultiCommit == True)
                {
                    operation = 'CreateOrAdjustTransaction';
                    type = 'AdjustTax';
                }
                else
                {
                    operation = 'CreateTransaction';
                    type = 'GetTax';
                }
                
                taxCalcInput.optionalParams = orderParam;
                connectorTimeRequestFetchStop = System.now();
                connectorFetchTime = connectorTimeRequestFetchStop.getTime() - connectorTimeStart.getTime();
                
                Ava_Mapper.TransactionModel transResult = order.calculateTax(taxCalcInput);
                connectorTimeResponseUpdateStart = System.now();
                if(transResult.statusCode == 200 || transResult.statusCode == 201)
                {
                    ConnectorTimeStop = System.now();
                    long connectorUpdateTime = ConnectorTimeStop.getTime() - ConnectorTimeResponseUpdateStart.getTime();
                    long connectorTime = connectorUpdateTime +connectorFetchTime;
                    logs = UtilityHelper.CreateGetTaxLogs(transResult, 'calculateTax()',string.valueof(connectorTime), sourceName,operation,type, null);
                    system.enqueueJob(new AVA_MAPPER.QueuableLogger(logs,UtilityHelper.getLogEntriesToken()));
                }
                else if(transResult.statusCode == 501)
                {
                    logs = UtilityHelper.CreateGetTaxLogs(transResult, 'calculateTax()','', sourceName,null,null, transResult.error.message);
                    logs = UtilityHelper.trimNewLineCharacterFromString(logs);
                    system.enqueueJob(new AVA_MAPPER.QueuableLogger(logs,UtilityHelper.getLogEntriesToken()));            
                }
            }
        }
        catch(Exception e)
        {
            logs = UtilityHelper.CreateGetTaxLogs(null, 'calculateTax()',null, sourceName,null,null, string.valueOf(e.getMessage()+','+e.getStackTraceString()));
            logs = UtilityHelper.trimNewLineCharacterFromString(logs);
            system.enqueueJob(new AVA_MAPPER.QueuableLogger(logs,UtilityHelper.getLogEntriesToken()));

            Order updateOrder = new Order(Id=orderId);
            if (Schema.sObjectType.Order.fields.AvaTax_Message__c.isUpdateable()){
                updateOrder.AvaTax_Message__c = e.getMessage()+','+e.getStackTraceString();
                if(!Test.isRunningTest())
                {
                	update updateOrder;
                }
            }
            //Throw new AVA_Mapper.AvaTaxException(e.getMessage()+','+e.getStackTraceString());
        }
    }
    
    @future (callout = true)
    public Static void cancelTax(Map<Id,String> mapOrderNumber)
    {
        if(!AvaTax__c.getInstance(AvaConstants.AVATAX_CUSTOM_SETTING_NAME).Disable_AvaTax_Triggers__c)
        {
            ConfigurationProvider config = new ConfigurationProvider();
            AVA_MAPPER.CancelTaxCalculator ctc = new AVA_MAPPER.CancelTaxCalculator(AvaConstants.STATIC_RESOURCE_FILENAME,AvaConstants.AVATAX_PACKAGE_NAMESPACE,config.hookExtension());

            System.debug('In CancelTax Trigger');
            for(Id orderId: mapOrderNumber.keySet())
            {
                List<String> orderNumberAndId = mapOrderNumber.get(orderId).split('--');
				System.debug('orderNumberAndId: '+orderNumberAndId);

                Map<String,String> oppParam = new Map<String,String>();
                AVA_MAPPER.QueryInput queryInput = new AVA_MAPPER.QueryInput();
                queryInput.recordId = orderNumberAndId[1];
                System.debug('orderNumberAndId[1]: '+orderNumberAndId[1]);
                
                queryInput.controller = orderId.getSObjectType().getDescribe().getName().toLowerCase();
                queryInput.optionalParams = oppParam;
    
                AVA_MAPPER.CancelTaxCalculationInput cancelTaxCalcInput = new AVA_MAPPER.CancelTaxCalculationInput();
                
                //cancelTaxCalcInput.companyCode = AvaTax__c.getInstance(AvaConstants.AVATAX_CUSTOM_SETTING_NAME).Company_Code__c;
                cancelTaxCalcInput.companyCode = UtilityHelper.getCompanyCodeForCancelTax(orderId);
                System.debug('cancelTaxCalcInput.companyCode: '+cancelTaxCalcInput.companyCode);
                
                cancelTaxCalcInput.transactionCode = orderNumberAndId[1];
                System.debug('orderId: '+orderNumberAndId);

                cancelTaxCalcInput.code = AVA_MAPPER.VoidReasonCode.DocVoided;
                cancelTaxCalcInput.docType = AVA_MAPPER.DocumentType.SalesInvoice;
                
                ctc.executeQuery(queryInput);
                Ava_Mapper.TransactionModel cancelTaxTransResult = ctc.cancelTax(cancelTaxCalcInput);
                System.debug('cancelTaxTransResult: '+cancelTaxTransResult);
            }
        }
    }

    public PageReference redirectPage() {
        // Redirect the user back to the original page         
        PageReference pageRef = new PageReference('/' + orderId);
        pageRef.setRedirect(true);
        return pageRef;
    }
}