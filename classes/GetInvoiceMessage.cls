Global class GetInvoiceMessage implements Ava_Mapper.IUpdateTaxHelper {
    Global String extension(Ava_Mapper.TransactionModel transResult, Integer lineIndex)
    {
        string updateInvoiceMessage = '';
        Map<String,String> productMap = new Map<String,String>();
        if(transResult.invoiceMessages != null)
        {
            for (Integer i = 0; i < transResult.invoiceMessages.size(); i++)
            {
                string lineNumber = '';
                for(string ln : transResult.invoiceMessages[i].lineNumbers )
                {
                    lineNumber += productMap.get(ln) + ',';
                }
                lineNumber = lineNumber.substring(0,lineNumber.length() - 1 );
                updateInvoiceMessage += transResult.invoiceMessages[i].content +'\n';
            }
        }
        else if(transResult.messages !=null)
        {
            for(AVA_MAPPER.AvaTaxMessage message : transResult.messages){
                if(message.summary.contains('Invoice  Messages'))
                {
                    AVA_MAPPER.InvoiceMessageDetail details =  (AVA_MAPPER.InvoiceMessageDetail) JSON.deserialize(message.details, AVA_MAPPER.InvoiceMessageDetail.class);
                    for(AVA_MAPPER.InvoiceMessageMasterList msgMaster : details.InvoiceMessageMasterList){
                        if(msgMaster.messageCode != '0' && msgMaster.message != ''){
                            updateInvoiceMessage += msgMaster.message;
                        }
                    }
                }
            }
        }
        return updateInvoiceMessage;
    }
}