<?xml version="1.0" encoding="UTF-8"?>
<CustomObject xmlns="http://soap.sforce.com/2006/04/metadata">
    <fields>
        <fullName>AvaTax_Doc_Status__c</fullName>
        <deprecated>false</deprecated>
        <description>Temporary = Not Saved to Avalara AvaTax
Saved = Saved but not Posted/Committed to Avalara AvaTax
Committed = Saved and Committed to Avalara AvaTax</description>
        <externalId>false</externalId>
        <inlineHelpText>Temporary = Not Saved to Avalara AvaTax
Saved = Saved but not Posted/Committed to Avalara AvaTax
Committed = Saved and Committed to Avalara AvaTax</inlineHelpText>
        <label>AvaTax Doc Status</label>
        <length>50</length>
        <required>false</required>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>AvaTax_Message__c</fullName>
        <deprecated>false</deprecated>
        <description>Sales Tax Not Current = Sales Tax Not Calculated or not Up To Date 
Sales Tax Current = Sales Tax Calculated &amp; Up To Date</description>
        <externalId>false</externalId>
        <inlineHelpText>Sales Tax Not Current = Sales Tax Not Calculated or not Up To Date 
Sales Tax Current = Sales Tax Calculated &amp; Up To Date</inlineHelpText>
        <label>AvaTax Message</label>
        <length>32768</length>
        <type>LongTextArea</type>
        <visibleLines>2</visibleLines>
    </fields>
    <fields>
        <fullName>Calculate_Tax__c</fullName>
        <defaultValue>false</defaultValue>
        <deprecated>false</deprecated>
        <description>If this checkbox is checked, AvaTax will perform automatic tax calculation for this transaction</description>
        <externalId>false</externalId>
        <inlineHelpText>If this checkbox is checked, AvaTax will perform automatic tax calculation for this transaction</inlineHelpText>
        <label>Calculate Tax</label>
        <type>Checkbox</type>
    </fields>
    <fields>
        <fullName>Entity_Use_Code__c</fullName>
        <deleteConstraint>SetNull</deleteConstraint>
        <deprecated>false</deprecated>
        <description>Please select an entity use code (exemption reason) for this customer.</description>
        <externalId>false</externalId>
        <inlineHelpText>Please select an entity use code (exemption reason) for this customer.</inlineHelpText>
        <label>Entity/Use Code</label>
        <referenceTo>AVA_MAPPER__Entity_Use_code__c</referenceTo>
        <relationshipLabel>Quotes</relationshipLabel>
        <relationshipName>Quotes</relationshipName>
        <required>false</required>
        <type>Lookup</type>
    </fields>
    <fields>
        <fullName>Invoice_Message__c</fullName>
        <deprecated>false</deprecated>
        <description>Invoices messages are typically only included in tax responses for VAT transactions. This is an Avalara output field only.</description>
        <externalId>false</externalId>
        <inlineHelpText>Invoices messages are typically only included in tax responses for VAT transactions. This is an Avalara output field only.</inlineHelpText>
        <label>Invoice Message</label>
        <length>32768</length>
        <type>LongTextArea</type>
        <visibleLines>2</visibleLines>
    </fields>
    <fields>
        <fullName>Is_Seller_Importer_Of_Record__c</fullName>
        <defaultValue>false</defaultValue>
        <deprecated>false</deprecated>
        <description>If checked, Seller is responsible for any import taxes and fees</description>
        <externalId>false</externalId>
        <inlineHelpText>If checked, Seller is responsible for any import taxes and fees</inlineHelpText>
        <label>Is Seller Importer Of Record</label>
        <type>Checkbox</type>
    </fields>
    <fields>
        <fullName>ShippingHandling_Tax__c</fullName>
        <deprecated>false</deprecated>
        <description>Sales Tax calculated for Shipping and Handling</description>
        <externalId>false</externalId>
        <inlineHelpText>Sales Tax calculated for Shipping and Handling</inlineHelpText>
        <label>Shipping and Handling Tax</label>
        <precision>18</precision>
        <required>false</required>
        <scale>2</scale>
        <type>Currency</type>
    </fields>
    <fields>
        <fullName>Tax_Date__c</fullName>
        <deprecated>false</deprecated>
        <description>This is the date the Avalara Service will use to determine the applicable tax rates for this Quote. If left blank, will be the same as the Quote Date.</description>
        <externalId>false</externalId>
        <inlineHelpText>This is the date the Avalara Service will use to determine the applicable tax rates for this Quote. If left blank, will be the same as the Quote Date.</inlineHelpText>
        <label>Tax Date</label>
        <required>false</required>
        <type>DateTime</type>
    </fields>
    <webLinks>
        <fullName>Calculate_Tax</fullName>
        <availability>online</availability>
        <displayType>button</displayType>
        <height>600</height>
        <linkType>page</linkType>
        <masterLabel>Calculate Tax</masterLabel>
        <openType>noSidebar</openType>
        <page>Quote_Calculate_Tax</page>
        <protected>false</protected>
    </webLinks>
    <webLinks>
        <fullName>Quote_Validate_Bill_To_Address</fullName>
        <availability>online</availability>
        <displayType>button</displayType>
        <encodingKey>UTF-8</encodingKey>
        <height>600</height>
        <linkType>url</linkType>
        <masterLabel>Validate Bill To Address</masterLabel>
        <openType>noSidebar</openType>
        <protected>false</protected>
        <url>/apex/AVA_MAPPER__addressValidation?id={!Quote.Id}&amp;sType=SFConfig&amp;adrType=Billing</url>
    </webLinks>
    <webLinks>
        <fullName>Quote_Validate_Ship_To_Address</fullName>
        <availability>online</availability>
        <displayType>button</displayType>
        <encodingKey>UTF-8</encodingKey>
        <height>600</height>
        <linkType>url</linkType>
        <masterLabel>Validate Ship To Address</masterLabel>
        <openType>noSidebar</openType>
        <protected>false</protected>
        <url>/apex/AVA_MAPPER__addressValidation?id={!Quote.Id}&amp;sType=SFConfig&amp;adrType=Shipping</url>
    </webLinks>
</CustomObject>
