({
    getAvaTaxConfiguration: function(component, page, recordToDisply,customObjName,customFieldName) {
        var action = component.get("c.getAvaTaxMultiEntityConfiguration");
        action.setParams({
            "pageNumber": page,
            "recordToDisply": recordToDisply,
            "customObjName":customObjName,
            "customFieldName":customFieldName
        });
        action.setCallback(this, function(a) {
            var result = a.getReturnValue();
            if($A.util.isUndefinedOrNull(result.cautionString))
            {
                component.set("v.companyOptions", result.optionList);
                component.set("v.shippingOptions", result.shippingCode);
                component.set("v.avataxConfiguration", result.avaTaxConfiguration);
                component.set("v.countryOption", result.countryListing);
                component.set("v.customObjName", result.customObjName);
                component.set("v.customFieldName", result.customFieldName);
                component.set("v.shippingSelectedValue", result.avaTaxConfiguration.AVA_SFCLOUD__Shipping_Code__c);
                component.set("v.companySelectedValue", result.avaTaxConfiguration.AVA_SFCLOUD__Company_Code__c);
                component.set("v.displayMultiMapping", result.displayMultiMapping);
                component.set("v.multiCompanyMappingMap", result.lstMultiCompanyMapping);	//CPQ+ - avataxConfiguration
                var iterationArr = [];
                
                for(var i=0;i<recordToDisply;i++)
                {
                    if(result.lstMultiCompanyMapping[i] != undefined)
                        iterationArr[i] = result.lstMultiCompanyMapping[i];
                }
                component.set("v.AccountsPagination", iterationArr);
                component.set("v.page", page);
                component.set("v.total", result.total);
                component.set("v.pages", Math.ceil(result.total / recordToDisply));
                component.set("v.SearchKeyWord","acc");
            }
            else
            {
                alert(result.cautionString);
            }    	
        });
        $A.enqueueAction(action);
    },
    
    navigate : function(component,page, recordToDisply) 
    {
        var totalRecords = component.get("v.multiCompanyMappingMap");
        
        var iterationArr1 = new Array();
        var offset = (page - 1) * recordToDisply;
        var nextCounter = parseInt(recordToDisply)+parseInt(offset);
        var counter =0;
        for(var i=offset;i<nextCounter;i++)
        {
            if(totalRecords[i] != undefined)
                iterationArr1[counter] = totalRecords[i];
            counter++;
        }
        component.set("v.AccountsPagination", iterationArr1);
        component.set("v.page", page);
        component.set("v.total", totalRecords.length);
        component.set("v.pages", Math.ceil(totalRecords.length / recordToDisply));
    },
    searchHelper : function(component,event,getInputkeyWord) {
        var action = component.get("c.fetchLookUpValues");
        action.setParams({
            'searchKeyWord': getInputkeyWord,
            'ObjectName' : component.get("v.objectAPIName")
        });
        action.setCallback(this, function(response) {
            $A.util.removeClass(component.find("mySpinner"), "slds-show");
            var state = response.getState();
            if (state === "SUCCESS") {
                var storeResponse = response.getReturnValue();
                if (storeResponse.length == 0) {
                    component.set("v.Message", 'No Result Found...');
                } else {
                    component.set("v.Message", '');
                }
                component.set("v.listOfSearchRecords", storeResponse);
            }
            
        });
        $A.enqueueAction(action);
        
    },
    
    checkMetaDataAPIConnection : function(component, orgUrl){
        var action = component.get("c.checkMetadataAPI");
        action.setParams({
            'url': orgUrl
        });
        action.setCallback(this, function(response) {
            $A.util.removeClass(component.find("mySpinner"), "slds-show");
            var state = response.getState();
            if (state === "SUCCESS") {
                var status = response.getReturnValue();
                if (status) {
                    component.set('v.toDisplayCreateRemoteSiteButton',false);
                } else {
                    component.set('v.toDisplayCreateRemoteSiteButton',true);
                }
            }
            
        });
        $A.enqueueAction(action);
    }
})