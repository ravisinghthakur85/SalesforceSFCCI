({
    handleUpdateEvent : function(component, event, helper) {
        var companyAddress = event.getParam("companyAddress");
        var subdiaryNameEvent = event.getParam("subdiaryName");
        var subdiaryNameLocal = component.get('v.subdiaryName');
        var showTooltipLocal = event.getParam("showTooltip");
        //console.log('showTooltipLocal in AvaCompany'+showTooltipLocal);
        console.log('companyAddress: '+companyAddress);
        if(subdiaryNameEvent==subdiaryNameLocal)
        {
            component.set("v.address",companyAddress);
            component.set("v.showTooltip",showTooltipLocal);
            component.set("v.tooltipValue","Please update the company address in AvaTax Companies Tab to use it as a “Ship From” address for tax calculation");
            
            console.log('tooltip value in companyadd:'+component.get("v.showTooltip"));
        }
    }
})